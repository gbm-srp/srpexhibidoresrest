package com.srp.ejb.beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.srp.cm.CMAccess;

/**
 * Session Bean implementation class ImagenesCMBean
 */
@Stateless
public class CMBean implements CMBeanLocal {

	// private static final Logger logger = LogManager.getLogger(Main.class.getName());

    /**
     * Default constructor. 
     */
    public CMBean() { }
    
    public boolean isLMFromContent(String prop_id) {
    	boolean result=false;
    	CMAccess cmacces=CMAccess.getInstance();
    	result=cmacces.isFromCM(prop_id);
    	return result;
    }
    
    public List<String> getListPaths(String elementID, int tipo) {
    	List<String> result;
    	CMAccess cmaccess=CMAccess.getInstance();
    	result=cmaccess.getListPaths(elementID, tipo);
    	return result;
    }
    
}
