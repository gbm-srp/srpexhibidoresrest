package com.srp.ejb.beans;

import java.util.Dictionary;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.srp.cm.CMAccess;
import com.srp.controlador.CConsulta;
import com.srp.dto.CMLibroMayorDTO;
import com.srp.dto.ConsultaFincaDTO;
import com.srp.dto.UsuarioConsultorDTO;

@Stateless
@LocalBean
public class ConsultaBean {

	public ConsultaBean() {
	}

	public List<UsuarioConsultorDTO> getUsuarioConsultor(String identificacion) {
		CConsulta cc = new CConsulta();
		return cc.getUsuarioConsultor(identificacion);
	}

	public String agregarConsulta(UsuarioConsultorDTO item) {
		CConsulta cc = new CConsulta();
		return cc.agregarConsulta(item);
	}

	public List<Dictionary<String, Object>> getDepartamentos(Integer plaza) {
		CConsulta cc = new CConsulta();
		return cc.getDepartamentos(plaza);
	}

	public boolean getValidaFinca(String finca) {
		CConsulta cc = new CConsulta();
		return cc.getValidaFinca(finca);
	}
	
	public List<List<Object>> getLibroMayor(String finca) {
		CConsulta cc = new CConsulta();
		return cc.getLibroMayor(finca);
	}
	
	public List<String> getLibroMayorPath(CMLibroMayorDTO item) {
		CConsulta cc = new CConsulta();
		return cc.getLibroMayorPath(item);
	}


	public List<List<Object>> getHistoriaFinca(String finca, Integer firmada) {
		CConsulta cc = new CConsulta();
		return cc.getHistoriaFinca(finca, firmada);
	}

	@SuppressWarnings("static-access")
	public List<String> getListPaths(String elementID, int tipo) {
		List<String> result;
		CMAccess cmaccess = CMAccess.getInstance();
		result = cmaccess.getListPaths(elementID, tipo);
		return result;
	}

	public boolean agregarConsultaFinca(ConsultaFincaDTO item) {
		CConsulta cc = new CConsulta();
		return cc.agregarConsultaFinca(item);
	}

	public boolean finalizarConsulta(Integer cons_id) {
		CConsulta cc = new CConsulta();
		return cc.finalizarConsulta(cons_id);
	}
	
	public Dictionary<String, Object> getConsultaDetalle(Integer cons_id) {
		CConsulta cc = new CConsulta();
		return cc.getConsultaDetalle(cons_id);
	}
	
	public double getAreaFinca(String finca) {
		CConsulta cc = new CConsulta();
		return cc.getAreaFinca(finca);
	}
	
	public List<String> getDocumentosEnTramite(String finca) {
		CConsulta cc = new CConsulta();
		return cc.getDocumentosEnTramite(finca);
	}
	
	public List<String> buscarFinca(String finca) {
		CConsulta cc = new CConsulta();
		return cc.buscarFinca(finca);
	}
	
	public List<UsuarioConsultorDTO> buscarPorNombre(String nombre) {
		CConsulta cc = new CConsulta();
		return cc.buscarPorNombre(nombre);
	}
	
	public List<UsuarioConsultorDTO> buscarPorDPI(String dpi) {
		CConsulta cc = new CConsulta();
		return cc.buscarPorDPI(dpi);
	}
	
	public boolean actualizarDatosConsultor(UsuarioConsultorDTO item) {
		CConsulta cc = new CConsulta();
		return cc.actualizarDatosUsuarioConsultor(item);
	}

}
