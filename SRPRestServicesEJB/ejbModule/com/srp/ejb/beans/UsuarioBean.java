package com.srp.ejb.beans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.srp.controlador.CUsuario;
import com.srp.dto.UsuarioDTO;

@Stateless
@LocalBean
public class UsuarioBean {

	public UsuarioBean() {
	}

	public UsuarioDTO login(String usuario, String clave) {
		CUsuario cu = new CUsuario();
		return cu.login(usuario, clave);
	}

}
