package srp.rest.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import com.srp.utils.RGPUtils;
import com.srp.controlador.CConsulta;
import com.srp.dto.CMLibroMayorDTO;
import com.srp.dto.ConsultaFincaDTO;
import com.srp.dto.UsuarioConsultorDTO;
import com.srp.ejb.beans.ConsultaBean;

@Path(value = "/consulta")
public class ConsultaRS {

	private final static Logger LOGGER = LogManager.getLogger(CConsulta.class.getName());

	@GET
	@Produces({ "application/json" })
	@Path(value = "usuarioConsultor/{identificacion}")
	public Response getUsuarioConsultor(@PathParam("identificacion") String identificacion) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.getUsuarioConsultor(identificacion));
		return Response.status(Response.Status.OK).entity(response).build();
	}

	@POST
	@Produces({ "application/json" })
	@Path(value = "agregar")
	public Response agregarConsulta(UsuarioConsultorDTO item) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		return Response.status(Response.Status.OK).entity(cb.agregarConsulta(item)).build();
	}

	@GET
	@Produces({ "application/json" })
	@Path(value = "departamentos/{plaza}")
	public Response getDepartamentos(@PathParam("plaza") Integer plaza) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.getDepartamentos(plaza));
		return Response.status(Response.Status.OK).entity(response).build();
	}

	@GET
	@Produces("application/json")
	@Path(value = "finca/{finca}")
	public Response getValidaFinca(@PathParam("finca") String finca) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		JSONObject result = new JSONObject();
		if (cb.getValidaFinca(finca))
			result.put("result", true);
		else
			result.put("result", false);
		return Response.status(Response.Status.OK).entity(result.toString()).build();
	}

	@GET
	@Produces("application/json")
	@Path(value = "libroMayor/{finca}")
	public Response getLibroMayor(@PathParam("finca") String finca) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.getLibroMayor(finca));
		return Response.status(Response.Status.OK).entity(response).build();
	}

	@POST
	@Produces({ "application/json" })
	@Path(value = "libroMayor/listPath")
	public Response getLibroMayorPath(CMLibroMayorDTO item) throws Exception {
		LOGGER.debug("getLibroMayorPath: " + item.toString());
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.getLibroMayorPath(item));
		return Response.status(Response.Status.OK).entity(response).build();
	}

	@GET
	@Produces({ "application/json" })
	@Path(value = "historiaFinca/{finca}")
	public Response getHistoriaFinca(@PathParam("finca") String finca) throws Exception {
		String response = null;
		ConsultaBean cb = new ConsultaBean();
		response = RGPUtils.list2json(cb.getHistoriaFinca(finca, 1)); // Solo las firmadas
		return Response.status(Response.Status.OK).entity(response).build();
	}

	@GET
	@Produces({ "application/json" })
	@Path(value = "getpathlist/{elementid}/{tipo}")
	public Response getPathList(@PathParam("elementid") String elementid, @PathParam("tipo") Integer tipo)
			throws Exception {
		String response = "ERROR";
		System.out.println("elementid " + elementid);
		System.out.println("tipo " + tipo);
		ConsultaBean cb = new ConsultaBean();
		List<String> result = cb.getListPaths(elementid, tipo);
		JSONArray jsArray2 = new JSONArray(result);
		response = jsArray2.toString();
		return Response.status(Response.Status.OK).entity(response).build();
	}

	@POST
	@Produces({ "application/json" })
	@Path(value = "consultaFinca/agregar")
	public Response agregarConsultaFinca8(ConsultaFincaDTO item) throws Exception {
		LOGGER.debug("agregarConsultaFinca: " + item.toString());
		JSONObject result = new JSONObject();
		ConsultaBean cb = new ConsultaBean();
		if (cb.agregarConsultaFinca(item))
			result.put("result", true);
		else
			result.put("result", false);
		return Response.status(Response.Status.OK).entity(result.toString()).build();
	}

	@POST
	@Produces({ "application/json" })
	@Path(value = "finalizar/{cons_id}")
	public Response finalizarConsulta(@PathParam("cons_id") Integer cons_id) throws Exception {
		JSONObject result = new JSONObject();
		ConsultaBean cb = new ConsultaBean();
		if (cb.finalizarConsulta(cons_id))
			result.put("result", true);
		else
			result.put("result", false);
		return Response.status(Response.Status.OK).entity(result.toString()).build();
	}

	@GET
	@Produces({ "application/json" })
	@Path(value = "detalle/{cons_id}")
	public Response getConsultaDetalle(@PathParam("cons_id") Integer cons_id) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		// Creating the ObjectMapper object
		ObjectMapper mapper = new ObjectMapper();
		// Converting the Object to JSONString
		String response = mapper.writeValueAsString(cb.getConsultaDetalle(cons_id));
		return Response.status(Response.Status.OK).entity(response).build();
	}
	
	@GET
	@Produces({ "application/json" })
	@Path(value = "areaFinca/{finca}")
	public Response getAreaFinca(@PathParam("finca") String finca) throws Exception {
		LOGGER.debug("agregarConsultaFinca: " + finca);
		JSONObject result = new JSONObject();
		ConsultaBean cb = new ConsultaBean();
		result.put("result", cb.getAreaFinca(finca));
		return Response.status(Response.Status.OK).entity(result.toString()).build();
	}
	
	@GET
	@Produces({ "application/json" })
	@Path(value = "documentosEnTramite/{finca}")
	public Response getDocumentosEnTramite(@PathParam("finca") String finca) throws Exception {
		LOGGER.debug("getDocumentosEnTramite: " + finca);
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.getDocumentosEnTramite(finca));
		return Response.status(Response.Status.OK).entity(response).build();
	}
	
	@GET
	@Produces({ "application/json" })
	@Path(value = "buscarFinca/{finca}")
	public Response buscarFinca(@PathParam("finca") String finca) throws Exception {
		LOGGER.debug("buscarFinca: " + finca);
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.buscarFinca(finca));
		return Response.status(Response.Status.OK).entity(response).build();
	}
	
	
	@GET
	@Produces({ "application/json" })
	@Path(value = "dpi/getPorNombre/{nombre}")
	public Response buscarPorNombre(@PathParam("nombre") String nombre) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.buscarPorNombre(nombre));
		return Response.status(Response.Status.OK).entity(response).build();
	}
	
	@GET
	@Produces({ "application/json" })
	@Path(value = "dpi/getPorDPI/{dpi}")
	public Response buscarPorDPI(@PathParam("dpi") String nombre) throws Exception {
		ConsultaBean cb = new ConsultaBean();
		String response = RGPUtils.list2json(cb.buscarPorDPI(nombre));
		return Response.status(Response.Status.OK).entity(response).build();
	}
	
	@PUT
	@Produces({ "application/json" })
	@Path(value = "actualizarDatosConsultor")
	public Response actualizarDatosConsultor(UsuarioConsultorDTO item) throws Exception {
		LOGGER.debug("actualizarUsuarioConsultor: " + item.toString());
		JSONObject result = new JSONObject();
		ConsultaBean cb = new ConsultaBean();
		if (cb.actualizarDatosConsultor(item))
			result.put("result", true);
		else
			result.put("result", false);
		return Response.status(Response.Status.OK).entity(result.toString()).build();
	}

}
