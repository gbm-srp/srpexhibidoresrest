package srp.rest.services;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class Inicio extends Application {
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(UsuarioRS.class);
		classes.add(ConsultaRS.class);
		return classes;
	}

}
