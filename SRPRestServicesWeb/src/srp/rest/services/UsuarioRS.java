package srp.rest.services;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.srp.ejb.beans.UsuarioBean;
import com.srp.utils.RGPUtils;

@Path(value = "/acceso")
public class UsuarioRS {

	@POST
	@Produces({ "application/json" })
	@Path(value = "autentica")
	public Response login(@FormParam("usuario") String usuario, @FormParam("clave") String clave)
			throws Exception {
		UsuarioBean ub = new UsuarioBean();
		String response = RGPUtils.object2json(ub.login(usuario.toUpperCase(), clave));
		return Response.status(Response.Status.OK).entity(response).build();
	}

}
