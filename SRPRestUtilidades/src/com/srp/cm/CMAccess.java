package com.srp.cm;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.mm.beans.CMBBaseConstant;
import com.ibm.mm.beans.CMBConnection;
import com.ibm.mm.beans.CMBDataManagement;
import com.ibm.mm.beans.CMBException;
import com.ibm.mm.beans.CMBItem;
import com.ibm.mm.beans.CMBNoConnectionException;
import com.ibm.mm.beans.CMBObject;
import com.ibm.mm.beans.CMBQueryService;
import com.ibm.mm.beans.CMBSearchResults;
import com.srp.cm.CMAccess;
import com.srp.dto.CMLibroMayorDTO;
import com.srp.utils.Configuration;

import sun.misc.BASE64Decoder;

public class CMAccess {

	private static final Logger LOGGER = LogManager.getLogger(CMAccess.class.getName());

	// ###DESARROLLO
	// private static final String rutaHttp = "http://172.16.0.56:82/"; 
	// ###PRODUCCIÓN
	private static final String rutaHttp = "http://172.15.10.16:82/";
	private static final String rutaFileSystem = "C://GBM/inscripciones/";

	private static CMAccess cmaccess;

	public static synchronized CMAccess getInstance() {
		if (cmaccess == null)
			cmaccess = new CMAccess();
		LOGGER.debug("access");
		return cmaccess;
	}

	public static boolean isFromCM(String prop_id) {
		boolean result = false;
		if (getCountFromCM(prop_id) > 0)
			result = true;
		return result;
	}

	private static int getCountFromCM(String prop_id) {
		int result = 0;
		CMBConnection icmConn = new CMBConnection();
		try {
			Configuration confDB = new Configuration("C:\\Configuration\\desa.conf");
			String connString;
			StringTokenizer connParts;
			BASE64Decoder b64d = new BASE64Decoder();
			byte[] connByte;
			icmConn.setDsType("ICM");
			connString = confDB.getProperty("rgpglsdb");
			connByte = b64d.decodeBuffer(connString);
			connParts = new StringTokenizer(new String(connByte), "|");
			String server = connParts.nextToken();
			String user = connParts.nextToken();
			String pwd = connParts.nextToken();
			icmConn.setServerName(server);
			icmConn.setUserid(user);
			icmConn.setPassword(pwd);
			icmConn.connect();
			CMBQueryService queryService = icmConn.getQueryService();
			CMBSearchResults searchResults = new CMBSearchResults();
			searchResults.setConnection(icmConn);
			queryService.setQueryString("/*[@PROP_ID=\"" + prop_id + "\"]", CMBBaseConstant.CMB_QS_TYPE_XPATH);
			queryService.setAsynchSearch(false);
			queryService.runQuery();
			searchResults.newResults(queryService.getResults());
			result = searchResults.getCount();
		} catch (Exception exc) {
			LOGGER.error("Error", exc);
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Saliendo del metodo ");
		return result;
	}

	public static List<String> getListPaths(String itemID, int tipo) {
		int partListSize = 0;
		CMBConnection icmConn = null;
		List<String> lista = new ArrayList<String>();
		try {
			icmConn = new CMBConnection();
			Configuration confDB = new Configuration("C:\\Configuration\\desa.conf");
			String connString;
			StringTokenizer connParts;
			BASE64Decoder b64d = new BASE64Decoder();
			byte[] connByte;
			icmConn = new CMBConnection();
			icmConn.setDsType("ICM");
			connString = confDB.getProperty("rgpglsdb");
			connByte = b64d.decodeBuffer(connString);
			connParts = new StringTokenizer(new String(connByte), "|");
			String server = connParts.nextToken();
			String user = connParts.nextToken();
			String pwd = connParts.nextToken();
			icmConn.setServerName(server);
			icmConn.setUserid(user);
			icmConn.setPassword(pwd);
			icmConn.connect();
			CMBQueryService queryService = icmConn.getQueryService();
			CMBSearchResults searchResults = new CMBSearchResults();
			searchResults.setConnection(icmConn);
			CMBDataManagement dataManagement = icmConn.getDataManagement();
			switch (tipo) {
			case 1:
				queryService.setQueryString("/*[@SUM_ID=\"" + itemID + "\"]", CMBBaseConstant.CMB_QS_TYPE_XPATH);
				break;
			case 2:
				queryService.setQueryString("/DOCUMENTS[@DOC_ID=\"" + itemID + "\"]",
						CMBBaseConstant.CMB_QS_TYPE_XPATH);
				break;
			case 3:
				queryService.setQueryString(
						"/*[@PROP_ID=\"" + itemID.substring(0, itemID.length() - 1) + "\" AND @PROP_PG=\""
								+ itemID.substring(itemID.length() - 1, itemID.length()) + "\"]",
						CMBBaseConstant.CMB_QS_TYPE_XPATH);
				break;
			case 4:
				queryService.setQueryString(
						"/*[@PROP_ID=\"" + itemID.substring(0, itemID.length() - 1) + "\" AND @PROP_PG=\""
								+ itemID.substring(itemID.length() - 1, itemID.length()) + "\"]",
						CMBBaseConstant.CMB_QS_TYPE_XPATH);
				break;
			case 5:
				queryService.setQueryString("/DOCUMENTOSHISTORICOS[@DOC_ID=\"" + itemID + "\"]",
						CMBBaseConstant.CMB_QS_TYPE_XPATH);
				break;
			case 6:
				queryService.setQueryString("(/ADJUNTOS[@DOC_ID=\"" + itemID + "\" AND @DOC_SEQ=1])",
						CMBBaseConstant.CMB_QS_TYPE_XPATH);
				break;
			}
			partListSize = 0;
			queryService.setAsynchSearch(false);
			queryService.runQuery();
			searchResults.newResults(queryService.getResults());
			int count = searchResults.getCount();
			LOGGER.debug("count: " + count);
			for (int aux = 0; aux < count; aux++) {
				// asumimos que es el resultado correcto
				CMBItem item = searchResults.getItem(aux);
				LOGGER.debug("item: " + item.getName());
				dataManagement.setDataObject(item);
				LOGGER.debug("dataManagement: " + dataManagement.getContentCount());
				partListSize = dataManagement.getContentCount();
				LOGGER.debug("partListSize: " + partListSize);
				for (int i = 0; i < partListSize; i++) {
					CMBObject part = dataManagement.getContent(i);
					LOGGER.debug("part: " + part.getOriginalFileName());
					ImageInputStream is = ImageIO.createImageInputStream(part.getDataStream());
					LOGGER.debug("is: " + is.length());
					if (is == null || is.length() == 0) {
						throw new IOException("Verificar imagen en content");
					}
					Iterator<ImageReader> iterator = ImageIO.getImageReaders(is);
					LOGGER.debug("iterator: " + iterator.toString());
					LOGGER.debug("iterator: " + iterator.hasNext());
					if (iterator == null || !iterator.hasNext()) {
						throw new IOException("La imagen no fue soportada");
					}
					// We are just looking for the first reader compatible:
					ImageReader reader = (ImageReader) iterator.next();
					iterator = null;
					reader.setInput(is);
					int nbPages = reader.getNumImages(true);
					for (int o = 0; o < nbPages; o++) {
						String path = rutaHttp + itemID + "_" + tipo + "_" + i + "_" + o + ".jpg";
						lista.add(path);
						// if (i == 0 && o == 0) { // Primer parte o primer pagina
						File f = new File(rutaFileSystem + itemID + "_" + tipo + "_" + i + "_" + o + ".jpg");
						// Check if the specified file
						// Exists or not
						if (f.exists()) {
							System.out.println("Existe ya no descargo");
						} else {
							System.out.println("Vamo a descaga solo la primera :D");
							BufferedImage img = reader.read(o);
							/*
							 * if(tipo == 1 && nbPages == 1 && partListSize == 1){//si solo es de una hoja
							 * la parte de una vez solo inscripciones CTmpDetalleCertis cdc=new
							 * CTmpDetalleCertis(); int tamanio = cdc.getTamanioUltimaImagen(itemID);
							 * 
							 * if(img.getHeight() >= (tamanio+50)){ img=img.getSubimage(0, 50,
							 * img.getWidth(), tamanio+50); } }
							 */
							ImageIO.write(img, "jpg",
									new File(rutaFileSystem + itemID + "_" + tipo + "_" + i + "_" + o + ".jpg"));
							img = null;
							if (tipo == 1) {
								BufferedImage initImage = ImageIO.read(
										new File(rutaFileSystem + itemID + "_" + tipo + "_" + i + "_" + o + ".jpg"));
								int width = initImage.getWidth(null);
								int height = initImage.getHeight(null);
								BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
								Graphics g = image.getGraphics();
								g.drawImage(initImage, 0, 0, null);
								int cortar = 0;
								boolean romper = false;
								for (int y = (height - 1); y >= 0; y--) {
									for (int x = (width - 1); x >= 0; x--) {
										int pixel = image.getRGB(x, y);
										if (pixel == 0xFF000000) {// encontro pixel negro
											romper = true;
											cortar = y;
											System.out.println(" altura : " + y);
											break;
										}
									}
									if (romper) {
										break;
									}
								}
								if (image.getHeight() >= cortar) {
									System.out.println(image.getWidth() + " " + (cortar - 10));
									initImage = initImage.getSubimage(0, 25, image.getWidth(), (cortar - 10));
								}
								ImageIO.write(initImage, "jpg",
										new File(rutaFileSystem + itemID + "_" + tipo + "_" + i + "_" + o + ".jpg"));
								initImage = null;
							}
						}
						// }
					}
				}
			}
			return lista;
		} catch (Exception exc) {
			lista = new ArrayList<String>();
			LOGGER.error("Error", exc);
			exc.printStackTrace();
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lista;
	}

	public static List<List<Object>> getLibroMayor(String finca) {
		CMBConnection icmConn = null;
		List<List<Object>> result = new ArrayList<List<Object>>();
		try {
			icmConn = new CMBConnection();
			Configuration confDB = new Configuration("C:\\Configuration\\desa.conf");
			String connString;
			StringTokenizer connParts;
			BASE64Decoder b64d = new BASE64Decoder();
			byte[] connByte;
			icmConn = new CMBConnection();
			icmConn.setDsType("ICM");
			connString = confDB.getProperty("rgpglsdb");
			connByte = b64d.decodeBuffer(connString);
			connParts = new StringTokenizer(new String(connByte), "|");
			String server = connParts.nextToken();
			String user = connParts.nextToken();
			String pwd = connParts.nextToken();
			icmConn.setServerName(server);
			icmConn.setUserid(user);
			icmConn.setPassword(pwd);
			icmConn.connect();
			CMBQueryService queryService = icmConn.getQueryService();
			CMBSearchResults searchResults = new CMBSearchResults();
			searchResults.setConnection(icmConn);
			queryService.setQueryString("/*[@PROP_ID=\"" + finca + "\" AND @PROP_PG IN (\"D\",\"H\")]",
					CMBBaseConstant.CMB_QS_TYPE_XPATH);
			queryService.setAsynchSearch(false);
			queryService.runQuery();
			searchResults.newResults(queryService.getResults());
			int count = searchResults.getCount();
			LOGGER.debug("count: " + count);
			ArrayList<Object> derechos = new ArrayList<Object>();
			ArrayList<Object> hipotecas = new ArrayList<Object>();
			for (int aux = 0; aux < count; aux++) {
				CMBItem item = searchResults.getItem(aux);
				Dictionary<String, Object> object = new Hashtable<String, Object>();
				for (int i = 0; i < item.getAttrCount(); i++) {
					object.put(item.getAttrName(i), item.getAttrValue(i).trim());
					LOGGER.debug("getAttrName: " + item.getAttrName(i));
					LOGGER.debug("getAttrValue: " + item.getAttrValue(i).trim());
				}
				// Derechos
				if (item.getAttrValue("PROP_PG").contains("D")) {
					derechos.add(object);
				}
				// Hipotecas
				else {
					hipotecas.add(object);
				}
			}
			result.add(derechos);
			result.add(hipotecas);
			// return result;
		} catch (Exception exc) {
			result = new ArrayList<List<Object>>();
			LOGGER.error("Error", exc);
			exc.printStackTrace();

			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public static List<String> getLibroMayorPath(CMLibroMayorDTO value) {
		int partListSize = 0;
		CMBConnection icmConn = null;
		List<String> lista = new ArrayList<String>();
		try {
			icmConn = new CMBConnection();
			Configuration confDB = new Configuration("C:\\Configuration\\desa.conf");
			String connString;
			StringTokenizer connParts;
			BASE64Decoder b64d = new BASE64Decoder();
			byte[] connByte;
			icmConn = new CMBConnection();
			icmConn.setDsType("ICM");
			connString = confDB.getProperty("rgpglsdb");
			connByte = b64d.decodeBuffer(connString);
			connParts = new StringTokenizer(new String(connByte), "|");
			String server = connParts.nextToken();
			String user = connParts.nextToken();
			String pwd = connParts.nextToken();
			icmConn.setServerName(server);
			icmConn.setUserid(user);
			icmConn.setPassword(pwd);
			icmConn.connect();
			CMBQueryService queryService = icmConn.getQueryService();
			CMBSearchResults searchResults = new CMBSearchResults();
			searchResults.setConnection(icmConn);
			CMBDataManagement dataManagement = icmConn.getDataManagement();
			queryService
					.setQueryString(
							"/*[@PROP_ID=\"" + value.getProp_id() + "\" AND @PROP_PG=\"" + value.getProp_pg()
									+ "\" AND @PAGINA=\"" + value.getPagina() + "\"]",
							CMBBaseConstant.CMB_QS_TYPE_XPATH);
			partListSize = 0;
			queryService.setAsynchSearch(false);
			queryService.runQuery();
			searchResults.newResults(queryService.getResults());
			int count = searchResults.getCount();
			LOGGER.debug("count: " + count);
			for (int aux = 0; aux < count; aux++) {
				CMBItem item = searchResults.getItem(aux);
				dataManagement.setDataObject(item);
				partListSize = dataManagement.getContentCount();
				LOGGER.debug("partListSize: " + partListSize);
				for (int i = 0; i < partListSize; i++) {
					CMBObject part = dataManagement.getContent(i);
					ImageInputStream is = ImageIO.createImageInputStream(part.getDataStream());
					if (is == null || is.length() == 0) {
						throw new IOException("Verificar imagen en content");
					}
					Iterator<ImageReader> iterator = ImageIO.getImageReaders(is);
					LOGGER.debug("iterator: " + iterator.toString());
					LOGGER.debug("iterator: " + iterator.hasNext());
					if (iterator == null || !iterator.hasNext()) {
						throw new IOException("La imagen no fue soportada");
					}
					ImageReader reader = (ImageReader) iterator.next();
					iterator = null;
					reader.setInput(is);
					int nbPages = reader.getNumImages(true);
					for (int o = 0; o < nbPages; o++) {
						String path = rutaHttp + value.getProp_id() + "_" + value.getProp_pg() + "_" + value.getPagina()
								+ "_" + i + "_" + o + ".jpg";
						lista.add(path);
						File f = new File(rutaFileSystem + value.getProp_id() + "_" + value.getProp_pg() + "_"
								+ value.getPagina() + "_" + i + "_" + o + ".jpg");
						if (f.exists()) {
							LOGGER.debug("Existe ya no descargo");
						} else {
							LOGGER.debug("Descargando");
							BufferedImage img = reader.read(o);
							ImageIO.write(img, "jpg", new File(rutaFileSystem + value.getProp_id() + "_"
									+ value.getProp_pg() + "_" + value.getPagina() + "_" + i + "_" + o + ".jpg"));
							img = null;
						}
					}
				}
			}
			return lista;
		} catch (Exception exc) {
			lista = new ArrayList<String>();
			LOGGER.error("Error", exc);
			exc.printStackTrace();
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			try {
				if (icmConn.isConnected()) {
					icmConn.disconnect();
				}
			} catch (CMBNoConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lista;
	}

}
