package com.srp.controlador;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.JSONObject;

import com.srp.dto.SummaryDTO;
import com.srp.cm.CMAccess;
import com.srp.db.Conexion;
import com.srp.dto.CMLibroMayorDTO;
import com.srp.dto.ConsultaDTO;
import com.srp.dto.ConsultaDocumentoDTO;
import com.srp.dto.ConsultaFincaDTO;
import com.srp.dto.UsuarioConsultorDTO;

public class CConsulta {

	private final static Logger LOGGER = LogManager.getLogger(CConsulta.class.getName());

	private Conexion conexion = null;

	public CConsulta() {
		conexion = new Conexion();
	}

	/* M�todos de Servicios */

	public List<UsuarioConsultorDTO> getUsuarioConsultor(String identificacion) {
		Connection conn = null;
		List<UsuarioConsultorDTO> result = null;
		try {
			conn = conexion.RafaConexion();
			result = getUsuarioConsultor_DB(conn, identificacion);
		} catch (Exception ex) {
			result = new ArrayList<UsuarioConsultorDTO>();
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}
	
	public List<UsuarioConsultorDTO> buscarPorNombre(String nombre) {
		Connection conn = null;
		List<UsuarioConsultorDTO> result = null;
		try {
			conn = conexion.RafaConexion();
			result = getUsuarioConsultorPorNombre_DB(conn, nombre);
		} catch (Exception ex) {
			LOGGER.error("ERROR: ", ex.getStackTrace().toString());
			result = new ArrayList<UsuarioConsultorDTO>();
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}
	
	public List<UsuarioConsultorDTO> buscarPorDPI(String dpi) {
		Connection conn = null;
		List<UsuarioConsultorDTO> result = null;
		try {
			conn = conexion.RafaConexion();
			result = getUsuarioConsultorPorDPI_DB(conn, dpi);
		} catch (Exception ex) {
			LOGGER.error("ERROR: ", ex.getStackTrace().toString());
			result = new ArrayList<UsuarioConsultorDTO>();
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}

	public String agregarConsulta(UsuarioConsultorDTO item) {
		Connection conn = null;
		// ConsultaDTO result = null;
		String result = "";
		try {
			conn = conexion.RafaConexion();
			// Crear consultor si no trae id
			if (item.getUcon_id() == null) {
				LOGGER.debug("agregarUsuarioConsultor - identificacion: " + item.getUcon_dpi());
				if (agregarUsuarioConsultor_DB(conn, item)) {
					LOGGER.debug("agregarUsuarioConsultor - Ok");
					// Obtener consultor
					List<UsuarioConsultorDTO> listUsuarioConsultor = new ArrayList<UsuarioConsultorDTO>();
					listUsuarioConsultor = getUsuarioConsultor_DB(conn, item.getUcon_dpi());
					LOGGER.debug("getUsuarioConsultor - identificacion: " + item.getUcon_dpi());
					if (listUsuarioConsultor.size() > 0) {
						item.setUcon_id(listUsuarioConsultor.get(0).getUcon_id());
					} else
						throw (new Exception("No se ha podido obtener la informaci�n del consultor."));
				} else
					throw (new Exception("No se ha podido agregar el consultor, por favor verifique sus datos."));
			} else {
				// Finalizar todas las consultas que tengan saldo 0
				List<Dictionary<String, Object>> consultas = getConsultaDetalleByUcon_DB(conn, item.getUcon_dpi());
				LOGGER.debug("consultas previas - count: " + consultas.size());
				for (Dictionary<String, Object> consulta : consultas) {
					double honorarios = Double.parseDouble(consulta.get("HONORARIOS").toString());
					Integer cons_id = Integer.parseInt(consulta.get("CONS_ID").toString());
					if (honorarios == 0) {
						if (!finalizarConsulta_DB(conn, cons_id, 3))
							throw (new Exception("No se pudieron finalizar la citas previas"));
					}
				}
				// Validar que el consultor no tenga saldo pendiente
				if (getValidaUsuarioConsultor_DB(conn, item.getUcon_dpi()))
					throw (new Exception(
							"El usuario tiene consultas pendientes por pagar, por favor dir�jase a cajas a cancelar el saldo pendiente."));
			}
			LOGGER.debug("getUsuarioConsultor - id: " + item.getUcon_id());
			// Crear consulta
			ConsultaDTO as = new ConsultaDTO();
			as.setCons_ucon_id(item.getUcon_id());
			as.setCons_ude_id(item.getCons_ude_id());
			if (agregarConsulta_DB(conn, as)) {
				List<ConsultaDTO> listConsulta = new ArrayList<ConsultaDTO>();
				listConsulta = getConsultaByUCon_DB(conn, item.getUcon_id());
				if (listConsulta.size() > 0) {
					LOGGER.debug("agregarConsulta - Ok");
					ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
					result = ow.writeValueAsString(listConsulta.get(0));
				} else
					throw new Exception("No se ha podido obtener el usuario consultor");
			} else
				throw new Exception("No se ha podido crear la consulta");
		} catch (Exception ex) {
			LOGGER.error("agregarConsulta - " + ex.getMessage());
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
			JSONObject errorObject = new JSONObject();
			errorObject.put("result", ex.getMessage());
			result = errorObject.toString();
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}

	public List<Dictionary<String, Object>> getDepartamentos(Integer plaza) {
		Connection conn = null;
		List<Dictionary<String, Object>> result = null;
		try {
			conn = conexion.PropConexion();
			result = getDepartamentos_DB(conn, plaza);
		} catch (Exception ex) {
			result = new ArrayList<Dictionary<String, Object>>();
			conexion.CerrarPropConexionRollback(conn);
			ex.printStackTrace();
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarPropConexionCommit(conn);
		}
		return result;
	}

	public boolean getValidaFinca(String finca) {
		Connection conn = null;
		boolean result = false;
		try {
			conn = conexion.PropConexion();
			result = getValidaFinca_DB(conn, finca);
			LOGGER.debug("MASTER_FINCAS-result: " + result);
			if (!result) {
				// Consulta en Content Manager
				// CMAccess cmacces = CMAccess.getInstance();
				result = CMAccess.isFromCM(finca);
				LOGGER.debug("CM-result: " + result);
				if (!result)
					throw new Exception("La finca no existe");
				else {
					// Guardar registro de consulta de finca
				}
			}
		} catch (Exception ex) {
			result = false;
			conexion.CerrarPropConexionRollback(conn);
			LOGGER.error(ex.getMessage());
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarPropConexionCommit(conn);
		}
		return result;
	}

	public List<List<Object>> getLibroMayor(String finca) {
		List<List<Object>> result = null;
		try {
			result = CMAccess.getLibroMayor(finca);
			LOGGER.debug("CM-result: " + result);
		} catch (Exception ex) {
			result = new ArrayList<List<Object>>();
			LOGGER.error(ex.getMessage());
		}
		return result;
	}

	public List<String> getLibroMayorPath(CMLibroMayorDTO item) {
		List<String> result = null;
		try {
			result = CMAccess.getLibroMayorPath(item);
			LOGGER.debug("CM-result: " + result.toString());
		} catch (Exception ex) {
			result = new ArrayList<String>();
			LOGGER.error(ex.getMessage());
		}
		return result;
	}

	public List<List<Object>> getHistoriaFinca(String finca, Integer firmada) {
		Connection conn = null;
		List<List<Object>> result = null;
		try {
			conn = conexion.PropConexion();
			result = getHistoriaFinca_DB(conn, finca, firmada);
		} catch (Exception ex) {
			result = new ArrayList<List<Object>>(8);
			conexion.CerrarPropConexionRollback(conn);
			// ex.printStackTrace();
			LOGGER.error(ex.getMessage());
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarPropConexionCommit(conn);
		}
		return result;
	}

	public boolean agregarConsultaFinca(ConsultaFincaDTO item) {
		Connection conn = null;
		boolean result = false;
		try {
			conn = conexion.RafaConexion();
			List<ConsultaFincaDTO> listConsultaFinca = getConsultaFinca_DB(conn, item);
			if (listConsultaFinca.isEmpty()) {
				// Agregar Q2 de honorarios por finca
				item.setCfi_honorarios(new BigDecimal(2));
				LOGGER.debug("agregarConsultaFinca - item: " + item.toString());
				if (agregarConsultaFinca_DB(conn, item))
					listConsultaFinca = getConsultaFinca_DB(conn, item);
				else
					throw new Exception("No se ha podido agregar la finca a la consulta");
			}
			// Agregar documento
			ConsultaDocumentoDTO consultaDocumento = new ConsultaDocumentoDTO();
			consultaDocumento.setCdo_cfi_id(listConsultaFinca.get(0).getCfi_id());
			consultaDocumento.setCdo_documento(item.getCdo_documento());
			consultaDocumento.setCdo_libro_mayor(item.getCdo_libro_mayor());
			BigDecimal honorExtra = new BigDecimal(0);
			// Validar el libro mayor
			String libroFinca = item.getCfi_finca().split("-")[2];
			LOGGER.debug("agregarConsultaFinca - libroFinca: -" + libroFinca + "-");
			LOGGER.debug("agregarConsultaFinca - item.getCdo_libro_mayor(): -" + item.getCdo_libro_mayor() + "-");
			if (item.getCdo_libro_mayor() != null && !libroFinca.contains(item.getCdo_libro_mayor())
					&& !getConsultaLibroMayor_DB(conn, consultaDocumento))
				honorExtra = new BigDecimal(1);
			consultaDocumento.setCdo_honor_extra(honorExtra);
			// Consulta por documento
			List<ConsultaDocumentoDTO> listConsultaDocumento = getConsultaDocumento_DB(conn, consultaDocumento);
			if (listConsultaDocumento.isEmpty() && agregarConsultaDocumento_DB(conn, consultaDocumento))
				result = true;
			else
				throw new Exception("No se ha podido agregar el documento a la consulta");
		} catch (Exception ex) {
			LOGGER.error("agregarConsultaFinca - ", ex);
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}

	public boolean finalizarConsulta(Integer cons_id) {
		Connection conn = null;
		boolean result = false;
		try {
			conn = conexion.RafaConexion();
			// Validar que la consulta no est� finalizada por cajas
			ConsultaDTO consulta;
			consulta = getConsultaById_DB(conn, cons_id);
			if (consulta != null) {
				if (consulta.getCons_estado() == 1) {
					if (finalizarConsulta_DB(conn, cons_id, 2)) {
						result = true;
					} else
						throw new Exception("No se ha podido finalizar la consulta");
				} else {
					// Si no tiene estado 1 ya est� finalizada o pagada.
					result = true;
				}
			} else
				throw new Exception("No se ha podido finalizar la consulta");
		} catch (Exception ex) {
			LOGGER.error("finalizarConsulta - " + ex.getMessage());
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}

	public Dictionary<String, Object> getConsultaDetalle(Integer cons_id) {
		Connection conn = null;
		Dictionary<String, Object> result = null;
		try {
			conn = conexion.RafaConexion();
			result = getConsultaDetalle_DB(conn, cons_id);
		} catch (Exception ex) {
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}
	
	public double getAreaFinca(String finca) {
		Connection conn = null;
		double result = 0;
		try {
			conn = conexion.PropConexion();
			result = getAreaFinca_DB(conn, finca);
		} catch (Exception ex) {
			LOGGER.error("getAreaFinca - " + ex.getMessage());
			conexion.CerrarPropConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarPropConexionCommit(conn);
		}
		return result;
	}
	
	public List<String> getDocumentosEnTramite(String finca) {
		Connection conn = null;
		List<String> result = null;
		try {
			conn = conexion.RafaGuateConexion();
			result = getDocumentosEnTramite_DB(conn,finca);
		} catch (Exception ex) {
			result = new ArrayList<String>();
			LOGGER.error(ex.getMessage());
			conexion.CerrarRafaGuateConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaGuateConexionCommit(conn);
		}
		return result;
	}
	
	public List<String> buscarFinca(String finca) {
		Connection conn = null;
		List<String> result = null;
		try {
			conn = conexion.PropConexion();
			result = buscarFinca_DB(conn,finca);
		} catch (Exception ex) {
			result = new ArrayList<String>();
			LOGGER.error(ex.getMessage());
			conexion.CerrarPropConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarPropConexionCommit(conn);
		}
		return result;
	}
	
	
	/* M�todos de Base de Datos */

	private static final String SQL_GET_USUARIO_CONSULTOR = "SELECT * FROM DORIAN.TBL_USUARIO_CONSULTOR"
			+ " WHERE UCON_DPI = ? AND UCON_ESTADO = 1";

	private List<UsuarioConsultorDTO> getUsuarioConsultor_DB(Connection conn, String identificacion) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<UsuarioConsultorDTO> result = null;
		ps = conn.prepareStatement(SQL_GET_USUARIO_CONSULTOR);
		try {
			ps.setString(1, identificacion);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<UsuarioConsultorDTO>();
				while (rs.next()) {
					UsuarioConsultorDTO a = new UsuarioConsultorDTO();
					a.setUcon_id(rs.getInt(1));
					a.setUcon_dpi(rs.getString(2));
					a.setUcon_nombre(rs.getString(3));
					a.setUcon_correo(rs.getString(4));
					a.setUcon_telefono(rs.getInt(5));
					a.setUcon_estado(rs.getInt(6));
					result.add(a);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}
	
	private static final String SQL_GET_USUARIO_CONSULTOR_POR_NOMBRE = "SELECT * FROM DORIAN.TBL_USUARIO_CONSULTOR WHERE LCASE(UCON_NOMBRE) LIKE CONCAT(CONCAT('%', LCASE(?)), '%')";

	private List<UsuarioConsultorDTO> getUsuarioConsultorPorNombre_DB(Connection conn, String nombre) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<UsuarioConsultorDTO> result = null;
		ps = conn.prepareStatement(SQL_GET_USUARIO_CONSULTOR_POR_NOMBRE);
		try {
			ps.setString(1, nombre);
			LOGGER.debug("DEBUG QUERY :", ps);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<UsuarioConsultorDTO>();
				while (rs.next()) {
					UsuarioConsultorDTO a = new UsuarioConsultorDTO();
					a.setUcon_id(rs.getInt(1));
					a.setUcon_dpi(rs.getString(2));
					a.setUcon_nombre(rs.getString(3));
					a.setUcon_correo(rs.getString(4));
					a.setUcon_telefono(rs.getInt(5));
					a.setUcon_estado(rs.getInt(6));
					result.add(a);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_USUARIO_CONSULTOR_POR_DPI = "SELECT * FROM DORIAN.TBL_USUARIO_CONSULTOR WHERE UCON_DPI LIKE CONCAT(CONCAT('%', ?), '%')";

	private List<UsuarioConsultorDTO> getUsuarioConsultorPorDPI_DB(Connection conn, String dpi) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<UsuarioConsultorDTO> result = null;
		ps = conn.prepareStatement(SQL_GET_USUARIO_CONSULTOR_POR_DPI);
		try {
			ps.setString(1, dpi);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<UsuarioConsultorDTO>();
				while (rs.next()) {
					UsuarioConsultorDTO a = new UsuarioConsultorDTO();
					a.setUcon_id(rs.getInt(1));
					a.setUcon_dpi(rs.getString(2));
					a.setUcon_nombre(rs.getString(3));
					a.setUcon_correo(rs.getString(4));
					a.setUcon_telefono(rs.getInt(5));
					a.setUcon_estado(rs.getInt(6));
					result.add(a);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	
	private static final String SQL_INSERT_USUARIO_CONSULTOR = "INSERT INTO DORIAN.TBL_USUARIO_CONSULTOR"
			+ " (UCON_DPI,UCON_NOMBRE,UCON_CORREO,UCON_TELEFONO,UCON_ESTADO) VALUES (?,?,?,?,1)";

	private boolean agregarUsuarioConsultor_DB(Connection conn, UsuarioConsultorDTO item) throws Exception {
		PreparedStatement ps = conn.prepareStatement(SQL_INSERT_USUARIO_CONSULTOR);
		ps.setString(1, item.getUcon_dpi());
		ps.setString(2, item.getUcon_nombre());
		ps.setString(3, item.getUcon_correo());
		ps.setInt(4, item.getUcon_telefono());
		try {
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}

	private static final String SQL_GET_CONSULTA_BY_UCON = "SELECT * FROM DORIAN.TRN_CONSULTA"
			+ " WHERE CONS_UCON_ID = ? AND CONS_ESTADO = 1 ORDER BY CONS_ID DESC";

	private List<ConsultaDTO> getConsultaByUCon_DB(Connection conn, Integer ucon_id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ConsultaDTO> result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_BY_UCON);
		try {
			ps.setInt(1, ucon_id);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<ConsultaDTO>();
				while (rs.next()) {
					ConsultaDTO a = new ConsultaDTO();
					a.setCons_id(rs.getInt(1));
					a.setCons_ude_id(rs.getString(2));
					a.setCons_ucon_id(rs.getInt(3));
					a.setCons_fecha_inicio(rs.getString(4));
					a.setCons_fecha_fin(rs.getString(5));
					a.setCons_estado(rs.getInt(6));
					result.add(a);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_INSERT_CONSULTA = "INSERT INTO DORIAN.TRN_CONSULTA"
			+ " (CONS_UDE_ID,CONS_UCON_ID,CONS_FECHA_INICIO,CONS_FECHA_FIN,CONS_ESTADO) VALUES (?,?,CURRENT_TIMESTAMP,NULL,1)";

	private boolean agregarConsulta_DB(Connection conn, ConsultaDTO item) throws Exception {
		PreparedStatement ps = conn.prepareStatement(SQL_INSERT_CONSULTA);
		ps.setString(1, item.getCons_ude_id());
		ps.setInt(2, item.getCons_ucon_id());
		try {
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}

	private static final String SQL_GET_DEPARTAMENTOS = "SELECT DEPT_NUM, DEPT_NAME FROM DORIAN.TBL_DEPT"
			+ " WHERE DEPT_PLAZA = ? ORDER BY DEPT_NUM ASC";

	private List<Dictionary<String, Object>> getDepartamentos_DB(Connection conn, Integer plaza) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Dictionary<String, Object>> result = null;
		ps = conn.prepareStatement(SQL_GET_DEPARTAMENTOS);
		ps.setInt(1, plaza);
		try {
			rs = ps.executeQuery();
			try {
				result = new ArrayList<Dictionary<String, Object>>();
				while (rs.next()) {
					Dictionary<String, Object> item = new Hashtable<String, Object>();
					item.put("DEPT_NUM", rs.getInt(1));
					item.put("DEPT_NAME", rs.getString(2));
					result.add(item);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_VALIDA_FINCA = "SELECT * FROM DORIAN.TBL_MASTER_FINCAS WHERE TRIM(MF_PROP_ID) = ?";

	private boolean getValidaFinca_DB(Connection conn, String finca) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		ps = conn.prepareStatement(SQL_GET_VALIDA_FINCA);
		ps.setString(1, finca);
		try {
			rs = ps.executeQuery();
			try {
				if (rs.next()) {
					result = true;
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			LOGGER.error("error: ", e);
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_HISTORIA_FINCA = "SELECT SUMM_PARENT_DOC_ID, SUMM_ID, "
			+ "SUMM_COL_ID, SUMM_COL_SEQ, SUMM_OPER_ID, SUMTMPL_NAME, SUMM_DATE, "
			+ "SUMM_SIGNED FROM DORIAN.TRN_SUMMARY, DORIAN.TBL_SUMM_TEMPLATE WHERE "
			+ "SUMM_COL_ID = SUMTMPL_COLUMN_ID AND SUMM_TEMP_OPT_ID = SUMTMPL_OPTION_ID "
			+ "AND SUMM_PROP_ID = ? AND SUMM_SIGNED = COALESCE(?, SUMM_SIGNED) ORDER BY SUMM_COL_ID, SUMM_COL_SEQ";

	private List<List<Object>> getHistoriaFinca_DB(Connection conn, String finca, Integer firmada) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<List<Object>> result = null;
		ps = conn.prepareStatement(SQL_GET_HISTORIA_FINCA);
		ps.setString(1, finca);
		if (firmada == 1)
			ps.setInt(2, firmada);
		else
			ps.setString(2, null);
		try {
			rs = ps.executeQuery();
			try {
				List<SummaryDTO> items = new ArrayList<SummaryDTO>();
				while (rs.next()) {
					SummaryDTO ma = new SummaryDTO();
					ma.setSumm_parent_doc_id(rs.getString(1));
					ma.setSumm_id(rs.getString(2));
					ma.setSumm_col_id(rs.getInt(3));
					ma.setSumm_col_seq(rs.getInt(4));
					ma.setSumm_oper_id(rs.getString(5));
					ma.setSumtmpl_name(rs.getString(6));
					ma.setSumm_date(rs.getDate(7));
					ma.setSumm_signed(rs.getInt(8));
					items.add(ma);
				}
				result = new ArrayList<List<Object>>();
				for (int i = 0; i < 8; i++) {
					ArrayList<Object> col = new ArrayList<Object>();
					for (SummaryDTO o : items) {
						if (o.getSumm_col_id() == i) {
							col.add(o);
						}
					}
					result.add(col);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_CONSULTA_FINCA = "SELECT * FROM DORIAN.TRN_CONSULTA_FINCA "
			+ "WHERE CFI_CONS_ID = ? AND CFI_FINCA = ?";

	private List<ConsultaFincaDTO> getConsultaFinca_DB(Connection conn, ConsultaFincaDTO item) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ConsultaFincaDTO> result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_FINCA);
		try {
			ps.setInt(1, item.getCfi_cons_id());
			ps.setString(2, item.getCfi_finca());
			rs = ps.executeQuery();
			try {
				result = new ArrayList<ConsultaFincaDTO>();
				while (rs.next()) {
					ConsultaFincaDTO a = new ConsultaFincaDTO();
					a.setCfi_id(rs.getInt(1));
					a.setCfi_cons_id(rs.getInt(2));
					a.setCfi_finca(rs.getString(3));
					a.setCfi_honorarios(rs.getBigDecimal(4));
					a.setCfi_estado(rs.getInt(5));
					result.add(a);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_INSERT_CONSULTA_FINCA = "INSERT INTO TRN_CONSULTA_FINCA (CFI_CONS_ID, CFI_FINCA, CFI_HONORARIOS, CFI_ESTADO) "
			+ "VALUES (?,?,?,1)";

	private boolean agregarConsultaFinca_DB(Connection conn, ConsultaFincaDTO item) throws Exception {
		PreparedStatement ps = conn.prepareStatement(SQL_INSERT_CONSULTA_FINCA);
		ps.setInt(1, item.getCfi_cons_id());
		ps.setString(2, item.getCfi_finca());
		ps.setBigDecimal(3, item.getCfi_honorarios());
		try {
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}

	private static final String SQL_FINALIZAR_CONSULTA = "UPDATE DORIAN.TRN_CONSULTA "
			+ "SET CONS_FECHA_FIN = CURRENT_TIMESTAMP, CONS_ESTADO = ? WHERE CONS_ID = ?";

	private boolean finalizarConsulta_DB(Connection conn, Integer cons_id, Integer cons_estado) throws Exception {
		PreparedStatement ps = conn.prepareStatement(SQL_FINALIZAR_CONSULTA);
		ps.setInt(1, cons_estado);
		ps.setInt(2, cons_id);
		try {
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}
	
	private static final String SQL_ACTUALIZAR_USUARIO_CONSULTOR= "UPDATE DORIAN.TBL_USUARIO_CONSULTOR "
			+ "SET UCON_DPI = ?, UCON_NOMBRE = ?, UCON_CORREO = ?, UCON_TELEFONO = ? WHERE UCON_ID = ?";

	private boolean actualizarUsuarioConsultor_DB(Connection conn, UsuarioConsultorDTO item) throws Exception {
		PreparedStatement ps = conn.prepareStatement(SQL_ACTUALIZAR_USUARIO_CONSULTOR);
		ps.setString(1, item.getUcon_dpi());
		ps.setString(2, item.getUcon_nombre());
		ps.setString(3, item.getUcon_correo());
		ps.setInt(4, item.getUcon_telefono());
		ps.setInt(5, item.getUcon_id());
		try {
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}

	private static final String SQL_GET_CONSULTA_DETALLE = "SELECT c.CONS_ID, c.CONS_FECHA_INICIO, "
			+ "c.CONS_FECHA_FIN, uc.UCON_NOMBRE, "
			+ "(SELECT COUNT(*) FROM DORIAN.TRN_CONSULTA_FINCA WHERE CFI_CONS_ID = c.CONS_ID) AS CANT_FICAS, "
			+ "(SELECT SUM(CFI_HONORARIOS) FROM DORIAN.TRN_CONSULTA_FINCA WHERE CFI_CONS_ID = c.CONS_ID) + "
			+ "(SELECT COALESCE(SUM(cd.CDO_HONOR_EXTRA), 0) FROM DORIAN.TRN_CONSULTA_FINCA cf "
			+ "INNER JOIN DORIAN.TRN_CONSULTA_DOCUMENTO cd ON cd.CDO_CFI_ID = cf.CFI_ID "
			+ "WHERE cf.CFI_CONS_ID = c.CONS_ID) AS HONORARIOS FROM DORIAN.TRN_CONSULTA c "
			+ "INNER JOIN DORIAN.TBL_USUARIO_CONSULTOR uc ON uc.UCON_ID = c.CONS_UCON_ID WHERE c.CONS_ID = ?";

	private Dictionary<String, Object> getConsultaDetalle_DB(Connection conn, Integer cons_id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Dictionary<String, Object> result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_DETALLE);
		try {
			ps.setInt(1, cons_id);
			rs = ps.executeQuery();
			try {
				result = new Hashtable<String, Object>();
				while (rs.next()) {
					result.put("CONS_ID", rs.getInt(1));
					result.put("CONS_FECHA_INICIO", rs.getString(2));
					if (rs.getString(3) != null)
						result.put("CONS_FECHA_FIN", rs.getString(3));
					else
						result.put("CONS_FECHA_FIN", "");
					result.put("UCON_NOMBRE", rs.getString(4));
					result.put("CANT_FICAS", rs.getInt(5));
					result.put("HONORARIOS", rs.getDouble(6));
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_CONSULTA_DOCUMENTO = "SELECT * FROM DORIAN.TRN_CONSULTA_DOCUMENTO "
			+ "WHERE CDO_CFI_ID = ? AND CDO_DOCUMENTO = COALESCE(?, CDO_DOCUMENTO)";

	private List<ConsultaDocumentoDTO> getConsultaDocumento_DB(Connection conn, ConsultaDocumentoDTO item)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ConsultaDocumentoDTO> result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_DOCUMENTO);
		try {
			ps.setInt(1, item.getCdo_cfi_id());
			ps.setString(2, item.getCdo_documento());
			rs = ps.executeQuery();
			try {
				result = new ArrayList<ConsultaDocumentoDTO>();
				while (rs.next()) {
					ConsultaDocumentoDTO a = new ConsultaDocumentoDTO();
					a.setCdo_id(rs.getInt(1));
					a.setCdo_cfi_id(rs.getInt(2));
					a.setCdo_documento(rs.getString(3));
					a.setCdo_libro_mayor(rs.getString(4));
					a.setCdo_estado(rs.getInt(5));
					result.add(a);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_CONSULTA_LIBRO_MAYOR = "SELECT * FROM DORIAN.TRN_CONSULTA_DOCUMENTO "
			+ "WHERE CDO_CFI_ID = ? AND CDO_LIBRO_MAYOR = COALESCE(?, CDO_LIBRO_MAYOR)";

	private boolean getConsultaLibroMayor_DB(Connection conn, ConsultaDocumentoDTO item) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_LIBRO_MAYOR);
		ps.setInt(1, item.getCdo_cfi_id());
		ps.setString(2, item.getCdo_libro_mayor());
		try {
			rs = ps.executeQuery();
			try {
				if (rs.next()) {
					result = true;
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			LOGGER.error("error: ", e);
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_INSERT_CONSULTA_DOCUMENTO = "INSERT INTO TRN_CONSULTA_DOCUMENTO (CDO_CFI_ID, CDO_DOCUMENTO, CDO_LIBRO_MAYOR, CDO_ESTADO, CDO_HONOR_EXTRA) "
			+ "VALUES (?,?,?,1,?)";

	private boolean agregarConsultaDocumento_DB(Connection conn, ConsultaDocumentoDTO item) throws Exception {
		PreparedStatement ps = conn.prepareStatement(SQL_INSERT_CONSULTA_DOCUMENTO);
		ps.setInt(1, item.getCdo_cfi_id());
		ps.setString(2, item.getCdo_documento());
		ps.setString(3, item.getCdo_libro_mayor());
		ps.setBigDecimal(4, item.getCdo_honor_extra());
		try {
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}

	private static final String SQL_GET_VALIDA_USUARIO_CONSULTOR = "SELECT * FROM DORIAN.TBL_USUARIO_CONSULTOR uc "
			+ "INNER JOIN DORIAN.TRN_CONSULTA c ON c.CONS_UCON_ID = uc.UCON_ID "
			+ "WHERE c.CONS_ESTADO IN (1,2) AND uc.UCON_DPI = ?";

	private boolean getValidaUsuarioConsultor_DB(Connection conn, String identificacion) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		ps = conn.prepareStatement(SQL_GET_VALIDA_USUARIO_CONSULTOR);
		ps.setString(1, identificacion);
		try {
			rs = ps.executeQuery();
			try {
				if (rs.next()) {
					result = true;
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			LOGGER.error("error: ", e);
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_CONSULTA_BY_ID = "SELECT * FROM DORIAN.TRN_CONSULTA WHERE CONS_ID = ?";

	private ConsultaDTO getConsultaById_DB(Connection conn, Integer cons_id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ConsultaDTO result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_BY_ID);
		try {
			ps.setInt(1, cons_id);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					result = new ConsultaDTO();
					result.setCons_id(rs.getInt(1));
					result.setCons_ude_id(rs.getString(2));
					result.setCons_ucon_id(rs.getInt(3));
					result.setCons_fecha_inicio(rs.getString(4));
					result.setCons_fecha_fin(rs.getString(5));
					result.setCons_estado(rs.getInt(6));
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

	private static final String SQL_GET_CONSULTA_DETALLE_BY_UCON = "SELECT c.CONS_ID, "
			+ "NVL(SUM((SELECT SUM(CFI_HONORARIOS) FROM DORIAN.TRN_CONSULTA_FINCA WHERE CFI_CONS_ID = c.CONS_ID) + "
			+ "(SELECT COALESCE(SUM(cd.CDO_HONOR_EXTRA), 0) FROM DORIAN.TRN_CONSULTA_FINCA cf "
			+ "INNER JOIN DORIAN.TRN_CONSULTA_DOCUMENTO cd ON cd.CDO_CFI_ID = cf.CFI_ID "
			+ "WHERE cf.CFI_CONS_ID = c.CONS_ID)),0) AS HONORARIOS " + "FROM DORIAN.TRN_CONSULTA c "
			+ "INNER JOIN DORIAN.TBL_USUARIO_CONSULTOR uc ON uc.UCON_ID = c.CONS_UCON_ID "
			+ "WHERE c.CONS_ESTADO IN (1,2) AND uc.UCON_DPI = ? " + "GROUP BY c.CONS_ID";

	private List<Dictionary<String, Object>> getConsultaDetalleByUcon_DB(Connection conn, String ucon_dpi)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Dictionary<String, Object>> result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_DETALLE_BY_UCON);
		try {
			ps.setString(1, ucon_dpi);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<Dictionary<String, Object>>();
				while (rs.next()) {
					Dictionary<String, Object> item = new Hashtable<String, Object>();
					item.put("CONS_ID", rs.getInt(1));
					item.put("HONORARIOS", rs.getDouble(2));
					result.add(item);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}
	
	private static final String SQL_GET_AREA_FINCA = "SELECT PROP_AREA FROM TBL_PROP_AREA WHERE PROP_ID=?";

	private double getAreaFinca_DB(Connection conn, String finca) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		double result = 0;
		ps = conn.prepareStatement(SQL_GET_AREA_FINCA);
		ps.setString(1, finca);
		try {
			rs = ps.executeQuery();
			try {
				if (rs.next()) {
					result = rs.getDouble(1);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} catch (Exception e) {
			LOGGER.error("error: ", e);
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}
	
	private static final String SQL_GET_CONSULTA_DOCS_TRAMITE = "SELECT DISTINCT btf_doc_id FROM dorian.trn_btch_finca WHERE btf_release IS NULL AND NOT EXISTS" +
            "(SELECT 1 FROM dorian.procesados WHERE docto = btf_doc_id AND UCASE(departamento) IN " +
            "('LISTO DEVOLVER','FIRMA', 'REGISTRADOR','DEVUELTO')) AND NOT EXISTS (SELECT 1 FROM dorian.trn_inst WHERE " +
            "inst_id  = btf_doc_id AND inst_type = 99 AND inst_clas=1) AND EXISTS (SELECT 1 from dorian.procesados WHERE docto = btf_doc_id ) " +
            " AND EXISTS (SELECT 1 FROM dorian.trn_inst WHERE inst_id = btf_doc_id AND INST_TYPE NOT IN (3,4,9)) AND btf_finca = ?";

	private List<String> getDocumentosEnTramite_DB(Connection conn, String finca)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> result = null;
		ps = conn.prepareStatement(SQL_GET_CONSULTA_DOCS_TRAMITE);
		try {
			ps.setString(1, finca);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<String>();
				while (rs.next()) {
					result.add(rs.getString(1));
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}
	
	private static final String SQL_BUSCAR_FINCA = "SELECT MF_PROP_ID " + 
			"FROM DORIAN.TBL_MASTER_FINCAS " + 
			"WHERE SUBSTR(MF_PROP_ID,LOCATE_IN_STRING(MF_PROP_ID,'-',-1)+ 1, " + 
			"	LENGTH(MF_PROP_ID)-LOCATE_IN_STRING(MF_PROP_ID,'-',-1)) IN ( " + 
			"		SELECT DEPT_NUM	FROM DORIAN.TBL_DEPT WHERE DEPT_PLAZA = 1) " + 
			"	AND MF_PROP_ID LIKE ?";

	private List<String> buscarFinca_DB(Connection conn, String finca)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> result = null;
		ps = conn.prepareStatement(SQL_BUSCAR_FINCA);
		try {
			ps.setString(1, '%' + finca.trim() + '%');
			rs = ps.executeQuery();
			try {
				result = new ArrayList<String>();
				while (rs.next()) {
					result.add(rs.getString(1).trim());
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}
	
	public boolean actualizarDatosUsuarioConsultor(UsuarioConsultorDTO item) {
		Connection conn = null;
		boolean result = false;
		try {
			conn = conexion.RafaConexion();
			// Validar que la consulta no est� finalizada por cajas
			if (actualizarUsuarioConsultor_DB(conn, item)) {
				result = true;
			} else
				throw new Exception("No se ha podido actualizar el usuario");
		
		} catch (Exception ex) {
			LOGGER.error("actualizando usuario consultor - " + ex.getMessage());
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}
	

}
