package com.srp.controlador;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.srp.db.Conexion;
import com.srp.dto.UsuarioDTO;
import com.srp.dto.UsuarioPerfilDTO;
import com.srp.utils.RGPUtils;

public class CUsuario {

	private static final Logger logger = LogManager.getLogger(CUsuario.class.getName());

	private Conexion conexion = null;

	public CUsuario() {
		conexion = new Conexion();
	}

	public UsuarioDTO login(String usuario, String clave) {
		Connection conn = null;
		UsuarioDTO result = null;
		try {
			conn = conexion.RafaConexion();
			String claveEncriptada = RGPUtils.getMD5(clave);
			logger.debug("login - usuario: " + usuario + ", intentanto iniciar sesi�n");
			result = verificaUsuario(conn, usuario, claveEncriptada);
			if (result == null)
				throw new Exception("No se ha podido obtener el usuario");
			// Asignar los perfiles del usuario
			result.setUsuarioPerfil(listaPerfilesUsuario(conn, usuario));
			logger.debug("login - resultado: " + result.toString());
		} catch (Exception ex) {
			logger.error("login - error: ", ex.getMessage());
			conexion.CerrarRafaConexionRollback(conn);
			conn = null;
		} finally {
			if (conn != null)
				conexion.CerrarRafaConexionCommit(conn);
		}
		return result;
	}

	public List<UsuarioPerfilDTO> listaPerfilesUsuario(String usuarioId) {
		Connection conn = null;
		List<UsuarioPerfilDTO> result = null;
		try {
			conn = conexion.PropConexion();
			result = listaPerfilesUsuario(conn, usuarioId);
		} catch (Exception ex) {
			logger.error("Error en listaMenuAplicacion ", ex);
			result = new ArrayList<UsuarioPerfilDTO>();
			conexion.CerrarPropConexionRollback(conn);
		} finally {
			if (conn != null)
				conexion.CerrarPropConexionCommit(conn);
		}
		return result;
	}

	private static final String SQL_SELECT_VERIFICA_USUARIO = "SELECT UDE_ID, UDE_CONTRASENA, UDE_NOMBRE, UDE_SUCURSAL "
			+ "FROM DORIAN.TBL_USUARIO_DESCRIPCION WHERE UPPER(UDE_ID) = ? AND UDE_CONTRASENA = ?";

	private UsuarioDTO verificaUsuario(Connection conn, String usuario, String clave) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;

		UsuarioDTO result = null;

		ps = conn.prepareStatement(SQL_SELECT_VERIFICA_USUARIO);
		ps.setString(1, usuario.trim().toUpperCase());
		ps.setString(2, clave);
		try {
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					result = new UsuarioDTO();
					result.setUde_id(rs.getString(1));
					result.setUde_clave(rs.getString(2));
					result.setUde_nombre(rs.getString(3));
					result.setUde_sucursal(rs.getString(4));
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}

		return result;
	}

	private static final String SQL_PERFILES_USUARIO = "SELECT p.PER_ID, p.PER_NOMBRE "
			+ "FROM DORIAN.TBL_USUARIO_PERFIL up INNER JOIN DORIAN.TBL_PERFIL p ON p.PER_ID = up.UPE_PER_ID "
			+ "WHERE up.UPE_ACTIVO = 1 AND up.UPE_UDE_ID = ? ORDER BY P.PER_NOMBRE";

	private List<UsuarioPerfilDTO> listaPerfilesUsuario(Connection conn, String usuarioId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<UsuarioPerfilDTO> result = null;
		ps = conn.prepareStatement(SQL_PERFILES_USUARIO);
		try {
			ps.setString(1, usuarioId);
			rs = ps.executeQuery();
			try {
				result = new ArrayList<UsuarioPerfilDTO>();
				while (rs.next()) {
					UsuarioPerfilDTO up = new UsuarioPerfilDTO();
					up.setPerId(rs.getInt(1));
					up.setPerNombre(rs.getString(2));
					result.add(up);
				}
			} finally {
				if (rs != null)
					rs.close();
			}
		} finally {
			if (ps != null)
				ps.close();
		}
		return result;
	}

}
