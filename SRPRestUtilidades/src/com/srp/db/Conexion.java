package com.srp.db;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Conexion extends Parametro implements PasosConexion{

	private static final Logger logger = LogManager.getLogger(Conexion.class.getName());
	
	/*** CONTROL DE CONEXIONES RAFA **/
	
	@Override
	public Connection RafaConexion() {
		Connection conn = null;
		try {
			//System.out.println("Crea conexion a RAFA");
			/*InitialContext context = new InitialContext();;
			DataSource dataSource = (DataSource) context.lookup(Parametro.JNDI_RAFA);
			conn = dataSource.getConnection();
			*/
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			// ** Desarrollo **
			 // String url = "jdbc:db2://172.10.10.28:50000/RGPGRAFA";
			// ** Producción **
			 String url = "jdbc:db2://172.15.10.88:50000/RGPGRAFA";
            conn = DriverManager.getConnection(url,"dorian", "d3t0m@z0");
            
			conn.setAutoCommit(false);
		} catch (Exception e) {
			logger.error("Error de SQL: ", e);
			return null;
		}
		return conn;
	}

	@Override
	public void CerrarRafaConexionCommit(Connection conn) {
		try {
			//System.out.println("Cierra conexion a RAFA con commit");
			conn.commit();
			conn.setAutoCommit(true);
			conn.close();
			conn = null;
		} catch (Exception ex) {
			logger.error("Error de SQL: ", ex);
		}
	}
	
	@Override
	public void CerrarRafaConexionRollback(Connection conn) {
		try {
			//System.out.println("Cierra conexion a RAFA con rollback");
			conn.rollback();
			conn.setAutoCommit(true);
			conn.close();
			conn = null;
		} catch (Exception ex) {
			logger.error("Error de SQL: ", ex);
		}
	}
	
	/*** CONTROL DE CONEXIONES PROP **/
	
	@Override
	public Connection PropConexion() {
		Connection conn = null;
		try {
			/*InitialContext context = new InitialContext();
			DataSource dataSource = (DataSource) context.lookup(Parametro.JNDI_PROP);
			conn = dataSource.getConnection();
			*/
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			// ** Desarrollo **
             // String url = "jdbc:db2://172.16.0.188:50000/RGPGPROP";
           //  conn = DriverManager.getConnection(url,"dorian", "Bd0mn3r");
			// ** Producción **
			String url = "jdbc:db2://172.16.0.181:60008/RGPGPROP";
            conn = DriverManager.getConnection(url,"dorian", "K03n&gs3gg");
            
			conn.setAutoCommit(false);
		} catch (Exception e) {
			logger.error("Error de SQL: ", e);
			return null;
		}
		return conn;
	}

	@Override
	public void CerrarPropConexionCommit(Connection conn) {
		try {
			conn.commit();
			conn.setAutoCommit(true);
			conn.close();
			conn = null;
		} catch (Exception ex) {
			logger.error("Error de SQL: ", ex);
		}
	}
	
	@Override
	public void CerrarPropConexionRollback(Connection conn) {
		try {
			conn.rollback();
			conn.setAutoCommit(true);
			conn.close();
			conn = null;
		} catch (Exception ex) {
			logger.error("Error de SQL: ", ex);
		}
	}
	
/*** CONTROL DE CONEXIONES RAFA **/
	
	@Override
	public Connection RafaGuateConexion() {
		Connection conn = null;
		try {
			//System.out.println("Crea conexion a RAFA");
			/*InitialContext context = new InitialContext();;
			DataSource dataSource = (DataSource) context.lookup(Parametro.JNDI_RAFA);
			conn = dataSource.getConnection();
			*/
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			// ** Producción **
			String url = "jdbc:db2://172.16.0.181:60008/RGPGRAFA";
            conn = DriverManager.getConnection(url,"dorian", "K03n&gs3gg"); 
            
			conn.setAutoCommit(false);
		} catch (Exception e) {
			logger.error("Error de SQL: ", e);
			return null;
		}
		return conn;
	}

	@Override
	public void CerrarRafaGuateConexionCommit(Connection conn) {
		try {
			//System.out.println("Cierra conexion a RAFA con commit");
			conn.commit();
			conn.setAutoCommit(true);
			conn.close();
			conn = null;
		} catch (Exception ex) {
			logger.error("Error de SQL: ", ex);
		}
	}
	
	@Override
	public void CerrarRafaGuateConexionRollback(Connection conn) {
		try {
			//System.out.println("Cierra conexion a RAFA con rollback");
			conn.rollback();
			conn.setAutoCommit(true);
			conn.close();
			conn = null;
		} catch (Exception ex) {
			logger.error("Error de SQL: ", ex);
		}
	}

}
