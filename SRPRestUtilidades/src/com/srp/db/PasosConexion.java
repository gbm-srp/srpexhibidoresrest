package com.srp.db;

import java.sql.Connection;

public interface PasosConexion {
	
	Connection RafaConexion();
	void CerrarRafaConexionCommit(Connection conn);
	void CerrarRafaConexionRollback(Connection conn);
	
	Connection PropConexion();
	void CerrarPropConexionCommit(Connection conn);
	void CerrarPropConexionRollback(Connection conn);
	
	Connection RafaGuateConexion();
	void CerrarRafaGuateConexionCommit(Connection conn);
	void CerrarRafaGuateConexionRollback(Connection conn);

}
