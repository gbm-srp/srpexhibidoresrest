package com.srp.dto;

import java.io.Serializable;

public class CMLibroMayorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String prop_id;

	private String prop_pg;

	private String pagina;

	public String getProp_id() {
		return prop_id;
	}

	public void setProp_id(String prop_id) {
		this.prop_id = prop_id;
	}

	public String getProp_pg() {
		return prop_pg;
	}

	public void setProp_pg(String prop_pg) {
		this.prop_pg = prop_pg;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	@Override
	public String toString() {
		return "{\"prop_id\":\"" + prop_id + "\", \"prop_pg\":\"" + prop_pg + "\", \"pagina\":\"" + pagina + "}";
	}

}
