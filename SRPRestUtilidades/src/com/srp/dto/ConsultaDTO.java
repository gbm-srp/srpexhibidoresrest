package com.srp.dto;

import java.io.Serializable;

public class ConsultaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer cons_id;

	private String cons_ude_id;

	private Integer cons_ucon_id;

	private String cons_fecha_inicio;

	private String cons_fecha_fin;

	private Integer cons_estado;

	public Integer getCons_id() {
		return cons_id;
	}

	public void setCons_id(Integer cons_id) {
		this.cons_id = cons_id;
	}

	public String getCons_ude_id() {
		return cons_ude_id;
	}

	public void setCons_ude_id(String cons_ude_id) {
		this.cons_ude_id = cons_ude_id;
	}

	public Integer getCons_ucon_id() {
		return cons_ucon_id;
	}

	public void setCons_ucon_id(Integer cons_ucon_id) {
		this.cons_ucon_id = cons_ucon_id;
	}

	public String getCons_fecha_inicio() {
		return cons_fecha_inicio;
	}

	public void setCons_fecha_inicio(String cons_fecha_inicio) {
		this.cons_fecha_inicio = cons_fecha_inicio;
	}

	public String getCons_fecha_fin() {
		return cons_fecha_fin;
	}

	public void setCons_fecha_fin(String cons_fecha_fin) {
		this.cons_fecha_fin = cons_fecha_fin;
	}

	public Integer getCons_estado() {
		return cons_estado;
	}

	public void setCons_estado(Integer cons_estado) {
		this.cons_estado = cons_estado;
	}

}
