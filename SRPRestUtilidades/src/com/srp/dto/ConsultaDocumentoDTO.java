package com.srp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConsultaDocumentoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer cdo_id;

	private Integer cdo_cfi_id;

	private String cdo_documento;

	private String cdo_libro_mayor;

	private Integer cdo_estado;
	
	private BigDecimal cdo_honor_extra;

	public Integer getCdo_id() {
		return cdo_id;
	}

	public void setCdo_id(Integer cdo_id) {
		this.cdo_id = cdo_id;
	}

	public Integer getCdo_cfi_id() {
		return cdo_cfi_id;
	}

	public void setCdo_cfi_id(Integer cdo_cfi_id) {
		this.cdo_cfi_id = cdo_cfi_id;
	}

	public String getCdo_documento() {
		return cdo_documento;
	}

	public void setCdo_documento(String cdo_documento) {
		this.cdo_documento = cdo_documento;
	}

	public String getCdo_libro_mayor() {
		return cdo_libro_mayor;
	}

	public void setCdo_libro_mayor(String cdo_libro_mayor) {
		this.cdo_libro_mayor = cdo_libro_mayor;
	}

	public Integer getCdo_estado() {
		return cdo_estado;
	}

	public void setCdo_estado(Integer cdo_estado) {
		this.cdo_estado = cdo_estado;
	}

	public BigDecimal getCdo_honor_extra() {
		return cdo_honor_extra;
	}

	public void setCdo_honor_extra(BigDecimal cdo_honor_extra) {
		this.cdo_honor_extra = cdo_honor_extra;
	}

	@Override
	public String toString() {
		return "{\"cdo_id\":\"" + cdo_id + "\", \"cdo_cfi_id\":\"" + cdo_cfi_id + "\", \"cdo_documento\":\"" + cdo_documento 
				+ "\", \"cdo_libro_mayor\":\"" + cdo_libro_mayor + "\", \"cdo_estado\":" + cdo_estado 
				+ "\", \"cdo_honor_extra\":\"" + cdo_honor_extra
				+ "}";
	}

}
