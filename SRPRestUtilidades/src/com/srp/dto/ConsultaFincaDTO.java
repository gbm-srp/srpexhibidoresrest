package com.srp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConsultaFincaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer cfi_id;

	private Integer cfi_cons_id;

	private String cfi_finca;

	private BigDecimal cfi_honorarios;

	private Integer cfi_estado;
	
	// Extras para request
	
	private String cdo_documento;

	private String cdo_libro_mayor;
	
	private BigDecimal cdo_honor_extra;

	public Integer getCfi_id() {
		return cfi_id;
	}

	public void setCfi_id(Integer cfi_id) {
		this.cfi_id = cfi_id;
	}

	public Integer getCfi_cons_id() {
		return cfi_cons_id;
	}

	public void setCfi_cons_id(Integer cfi_cons_id) {
		this.cfi_cons_id = cfi_cons_id;
	}

	public String getCfi_finca() {
		return cfi_finca;
	}

	public void setCfi_finca(String cfi_finca) {
		this.cfi_finca = cfi_finca;
	}

	public BigDecimal getCfi_honorarios() {
		return cfi_honorarios;
	}

	public void setCfi_honorarios(BigDecimal cfi_honorarios) {
		this.cfi_honorarios = cfi_honorarios;
	}

	public Integer getCfi_estado() {
		return cfi_estado;
	}

	public void setCfi_estado(Integer cfi_estado) {
		this.cfi_estado = cfi_estado;
	}
	
	public String getCdo_documento() {
		return cdo_documento;
	}

	public void setCdo_documento(String cdo_documento) {
		this.cdo_documento = cdo_documento;
	}

	public String getCdo_libro_mayor() {
		return cdo_libro_mayor;
	}

	public void setCdo_libro_mayor(String cdo_libro_mayor) {
		this.cdo_libro_mayor = cdo_libro_mayor;
	}

	public BigDecimal getCdo_honor_extra() {
		return cdo_honor_extra;
	}

	public void setCdo_honor_extra(BigDecimal cdo_honor_extra) {
		this.cdo_honor_extra = cdo_honor_extra;
	}

	@Override
	public String toString() {
		return "{\"cfi_id\":\"" + cfi_id + "\", \"cfi_cons_id\":\"" + cfi_cons_id + "\", \"cfi_finca\":\"" + cfi_finca
				+ "\", \"cfi_honorarios\":\"" + cfi_honorarios + "\", \"cfi_estado\":" + cfi_estado + "}";
	}

}
