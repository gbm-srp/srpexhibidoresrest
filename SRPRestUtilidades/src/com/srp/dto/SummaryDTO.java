package com.srp.dto;

import java.io.Serializable;
import java.sql.Date;

public class SummaryDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String summ_parent_doc_id;
	
	private String summ_id;
	
	private Integer summ_col_id;
	
	private Integer summ_col_seq;
	
	private String summ_oper_id;
	
	private Date summ_date;
	
	private Integer summ_signed;
	
	// Atributos extras
	
	private String sumtmpl_name;
	
	private boolean summ_selected;
	
	// Getters and Setters

	public String getSumm_parent_doc_id() {
		return summ_parent_doc_id;
	}

	public void setSumm_parent_doc_id(String summ_parent_doc_id) {
		this.summ_parent_doc_id = summ_parent_doc_id;
	}

	public String getSumm_id() {
		return summ_id;
	}

	public void setSumm_id(String summ_id) {
		this.summ_id = summ_id;
	}

	public Integer getSumm_col_id() {
		return summ_col_id;
	}

	public void setSumm_col_id(Integer summ_col_id) {
		this.summ_col_id = summ_col_id;
	}

	public Integer getSumm_col_seq() {
		return summ_col_seq;
	}

	public void setSumm_col_seq(Integer summ_col_seq) {
		this.summ_col_seq = summ_col_seq;
	}

	public String getSumm_oper_id() {
		return summ_oper_id;
	}

	public void setSumm_oper_id(String summ_oper_id) {
		this.summ_oper_id = summ_oper_id;
	}

	public Date getSumm_date() {
		return summ_date;
	}

	public void setSumm_date(Date summ_date) {
		this.summ_date = summ_date;
	}

	public Integer getSumm_signed() {
		return summ_signed;
	}

	public void setSumm_signed(Integer summ_signed) {
		this.summ_signed = summ_signed;
	}

	public String getSumtmpl_name() {
		return sumtmpl_name;
	}

	public void setSumtmpl_name(String sumtmpl_name) {
		this.sumtmpl_name = sumtmpl_name;
	}

	public boolean isSumm_selected() {
		return summ_selected;
	}

	public void setSumm_selected(boolean summ_selected) {
		this.summ_selected = summ_selected;
	}
	
}
