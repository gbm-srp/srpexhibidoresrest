package com.srp.dto;

import java.io.Serializable;

public class UsuarioConsultorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer ucon_id;

	private String ucon_dpi;

	private String ucon_nombre;

	private String ucon_correo;

	private Integer ucon_telefono;

	private Integer ucon_estado;
	
	// Extras para request
	
	private String cons_ude_id;

	public Integer getUcon_id() {
		return ucon_id;
	}

	public void setUcon_id(Integer ucon_id) {
		this.ucon_id = ucon_id;
	}

	public String getUcon_dpi() {
		return ucon_dpi;
	}

	public void setUcon_dpi(String ucon_dpi) {
		this.ucon_dpi = ucon_dpi;
	}

	public String getUcon_nombre() {
		return ucon_nombre;
	}

	public void setUcon_nombre(String ucon_nombre) {
		this.ucon_nombre = ucon_nombre;
	}

	public String getUcon_correo() {
		return ucon_correo;
	}

	public void setUcon_correo(String ucon_correo) {
		this.ucon_correo = ucon_correo;
	}

	public Integer getUcon_telefono() {
		return ucon_telefono;
	}

	public void setUcon_telefono(Integer ucon_telefono) {
		this.ucon_telefono = ucon_telefono;
	}

	public Integer getUcon_estado() {
		return ucon_estado;
	}

	public void setUcon_estado(Integer ucon_estado) {
		this.ucon_estado = ucon_estado;
	}

	public String getCons_ude_id() {
		return cons_ude_id;
	}

	public void setCons_ude_id(String cons_ude_id) {
		this.cons_ude_id = cons_ude_id;
	}

	@Override
	public String toString() {
		return "{\"ucon_id\":\"" + ucon_id + "\", \"ucon_dpi\":\"" + ucon_dpi + "\", \"ucon_nombre\":\"" + ucon_nombre
				+ "\", \"ucon_correo\":\"" + ucon_correo + "\", \"ucon_telefono\":" + ucon_telefono
				+ "\", \"ucon_estado\":" + ucon_estado + "}";
	}

}
