package com.srp.dto;

import java.io.Serializable;
import java.util.List;

public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ude_id;
	private String ude_clave;
	private String ude_nombre;
	private String ude_sucursal;
	private List<UsuarioPerfilDTO> usuarioPerfil;

	public String getUde_id() {
		return ude_id;
	}

	public void setUde_id(String ude_id) {
		this.ude_id = ude_id;
	}

	public String getUde_clave() {
		return ude_clave;
	}

	public void setUde_clave(String ude_clave) {
		this.ude_clave = ude_clave;
	}

	public String getUde_nombre() {
		return ude_nombre;
	}

	public void setUde_nombre(String ude_nombre) {
		this.ude_nombre = ude_nombre;
	}

	public String getUde_sucursal() {
		return ude_sucursal;
	}

	public void setUde_sucursal(String ude_sucursal) {
		this.ude_sucursal = ude_sucursal;
	}

	public List<UsuarioPerfilDTO> getUsuarioPerfil() {
		return usuarioPerfil;
	}

	public void setUsuarioPerfil(List<UsuarioPerfilDTO> usuarioPerfil) {
		this.usuarioPerfil = usuarioPerfil;
	}

	@Override
	public String toString() {
		return "{\"ude_id\":\"" + ude_id + "\", \"ude_clave\":\"" + ude_clave + "\", \"ude_nombre\":\"" + ude_nombre
				+ "\", \"ude_sucursal\":\"" + ude_sucursal + "}";
	}

}