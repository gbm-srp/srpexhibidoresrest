package com.srp.dto;

import java.io.Serializable;

public class UsuarioPerfilDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int perId;
	private String perNombre;
	// private String upeUdeId;
	// private String upeBandeja;
	// private int upeActivo;

	public int getPerId() {
		return perId;
	}

	public void setPerId(int perId) {
		this.perId = perId;
	}

	public String getPerNombre() {
		return perNombre;
	}

	public void setPerNombre(String perNombre) {
		this.perNombre = perNombre;
	}

	/* public String getUpeUdeId() {
		return upeUdeId;
	}

	public void setUpeUdeId(String upeUdeId) {
		this.upeUdeId = upeUdeId;
	}

	public String getUpeBandeja() {
		return upeBandeja;
	}

	public void setUpeBandeja(String upeBandeja) {
		this.upeBandeja = upeBandeja;
	}

	public int getUpeActivo() {
		return upeActivo;
	}

	public void setUpeActivo(int upeActivo) {
		this.upeActivo = upeActivo;
	}*/

	@Override
	public String toString() {
		return "[perId:" + perId + ", perNombre:" + perNombre 
				// + ", upeUdeId:" + upeUdeId + ", upeBandeja:" + upeBandeja
				// + ", upeActivo:" + upeActivo 
				+ "]";
	}

}
