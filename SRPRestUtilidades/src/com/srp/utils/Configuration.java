package com.srp.utils;

import java.io.FileInputStream;
import java.util.Properties;

import sun.misc.BASE64Encoder;

/**
 *
 * @author majurado
 */
public class Configuration {
	Properties prop = new Properties();
	FileInputStream fis = null;
	BASE64Encoder B64Enc = new BASE64Encoder();

	public Configuration(String strFileName) {
		if (!strFileName.equals("")) {
			try {
				fis = new FileInputStream(strFileName);
				prop.load(fis);
			} catch (Exception ex) {
			}
		}
	}

	public String getPropertyCoded(String propertyName) {
		String mensaje = null;
		String result = "";

		try {
			mensaje = prop.getProperty(propertyName);
			result = B64Enc.encode(mensaje.getBytes());
		} catch (Exception ex) {
		}

		return result;
	}

	public String getProperty(String propertyName) {
		String result = "";

		try {
			System.out.println("Property: " + propertyName);
			result = prop.getProperty(propertyName);
			System.out.println("Resultado: " + result);
		} catch (Exception ex) {
			System.out.println("ERROR : " + ex.getMessage());
		}

		return result;
	}
}