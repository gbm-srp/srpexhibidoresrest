package com.srp.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

public class RGPUtils {

	public static String getMD5(String clave) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(clave.trim().getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            return number.toString(16);
        } catch (Exception ex) {
            return null;
        }
    }
	
	public static String object2json(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonData = mapper.writeValueAsString(object);
        return jsonData;
    }
	
	@SuppressWarnings("unchecked")
	public static <T> T json2object(String jsonData, Class<?> clase) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
		T readValue = ((T) objectMapper.readValue(jsonData, Class.forName(clase.getName())));
		return readValue;        
    }
	
	public static String list2json(List<?> object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonData = mapper.writeValueAsString(object);
        return jsonData;
    }
	
	public static <T> List<T> json2list(String json, Class<?> clase) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		TypeFactory typeFactory = objectMapper.getTypeFactory();
		List<T> fincas = objectMapper.readValue(json,
				typeFactory.constructCollectionType(List.class, Class.forName(clase.getName())));
		return fincas;
	}
	
}
